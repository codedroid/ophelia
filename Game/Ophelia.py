import subprocess
import json
import os
# ----------------------------------------------------------------
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ----------------------------------------------------------------
import sys
import math
import time
import random
import msvcrt
from colorama import Fore, init
import random
import pickle
import multiprocessing
import keyboard
init()


class Item(object):
    def __init__(self, dict_):
        self.name="Noname"
        self.description="No description"
        self.comment="No comment"
        self.price=0
        self.level=1
        self.strength=1
        self.damage=1
        self.mana=0
        self.__dict__.update(dict_)
        self.rare=rare(self.level)
        exec(self.__dict__["exec"])
        del self.exec
    def action(self):
        loc = {"ret":"","self":self}
        if "equipment" in self.__dict__:
            codes=[self.interact]
            for item in self.__dict__["equipment"]:
                loc_t = {"ret":"","self":self,"item":item}
                exec("item.action()",globals(),loc_t)
                codes.append(loc_t["ret"])
            codes="\n".join(codes)
            exec(codes, globals(),loc)
        else:
            exec(self.interact, globals(),loc)
        ret=loc["ret"]
        return ret
    def deep_attr(self,attr):
        if attr in self.__dict__:
            a=self.__dict__[attr]
        else:
            a=0
        if "equipment" in self.__dict__:
            for item in self.__dict__["equipment"]:
                a+=item.deep_attr(attr)
        return a

    def __repr__(self):return "╔══════════════════════════╗\n"+str(f"{self.rare} {self.name}\n"+"║  >"+"\n║  >".join(list(map(lambda x: str(x[0])+" = "+str(x[1]),self.__dict__.items())))+"\n╚══════════════════════════╝"+Fore.RESET)
def dict2obj(d):
    return json.loads(json.dumps(d), object_hook=Item)

def drop(level):
    items=json.load(open("items.json","r",encoding='utf-8'))
    item=random.choice(items)
    item.update({"level":int(max(0,random.normalvariate(level, level)))}) #Распледеление гаусса
    if "damage" in item: item["damage"]*=item["level"]*random.random()+0.5
    if "strength" in item: item["strength"]*=item["strength"]*random.random()+0.5
    if "price" in item: item["price"]*=item["price"]*random.random()+0.5
    if "luck" in item: item["luck"]*=item["luck"]*random.random()+0.5
    if "mana" in item: item["mana"]*=item["mana"]*random.random()+0.5
    obj=dict2obj(item)
    return obj

def sacrifice(owner=None,victim=None):
    if "equipment" in owner.__dict__:
        owner.equipment.append(victim)
    else:
        owner.equipment=[victim]

"""
def read(name):
    zipObj = zipfile.ZipFile(f'{name}.png', 'r')
    data = zipObj.read('key.json')
    zipObj.close()
    return dict2obj(json.loads(data))
def create(name,type,d):
    zipObj = zipfile.ZipFile('key.zip', 'w')
    zipObj.writestr('key.json', json.dumps(d))
    zipObj.close()
    os.system(f"copy /b {type}.png + key.zip {name}.png")
    os.system(f"del key.zip")

def QTE(hard): #ОТ -бесконечность до +бесконечность

    print("================================ Hit the Right key =================================")
    letters="qwertyuiopasdfghjklzxcvbnm"
    letters=letters+letters.upper()
    tries=[]
    for _ in range(random.randint(5,10)):
        st=time.time()
        while 1:
            tr=random.choice(letters)
            print(f"  < [{tr}] \x1B[1A")
            ch=msvcrt.getch().decode()
            if ch==tr: break
        tries.append(time.time()-st)
        #print("\r\r")
    return (1/(sum(tries)/len(tries)))**4
"""
def rare(power):
    power = max(math.log(power+0.00000000000001),0)
    rares=[f"{Fore.WHITE}Хламовый",f"{Fore.GREEN}Обычный",f"{Fore.BLUE}Редкий",f"{Fore.CYAN}Уникальный",f"{Fore.MAGENTA}Мифический",f"{Fore.YELLOW}Легендарный",f"{Fore.LIGHTCYAN_EX}Лунный",f"{Fore.RED}Святой",f"{Fore.LIGHTMAGENTA_EX}Межпространственный",f"{Fore.LIGHTGREEN_EX}Хаотичный",f"{Fore.LIGHTRED_EX}Реликтовый",f"{Fore.LIGHTGREEN_EX}Галактический",f"{Fore.LIGHTGREEN_EX}Межгалактический",f"{Fore.LIGHTGREEN_EX}Супер-пупер",f"{Fore.LIGHTGREEN_EX}Супер-ультра",f"{Fore.LIGHTGREEN_EX}Гипер-квадро-мега",f"{Fore.YELLOW}Божественный",f"{Fore.BLACK}Пустотный",f"{Fore.WHITE}ЧИТЕР!",f"{Fore.WHITE}ХАКЕР!",f"{Fore.WHITE}Разработчик?",f"{Fore.BLACK}Неопределено",f"{Fore.BLACK}Недостижимо",f"{Fore.BLACK}Невозможно",f"{Fore.RED}Math error",f"{Fore.BLUE}Armageddon",f"{Fore.GREEN}Game Over",f"{Fore.LIGHTBLACK_EX}Ливни из игры, сука"]
    return '['+rares[min(int(power),27)]+']'

def clean(lines):
    print(f"\x1B[{lines}A")
    print(lines*"                                                                                                                            \n",end="")
    print(f"\x1B[{lines}A")



def findClosestValue(givenList, target): return min(givenList, key=lambda item: abs(item - target))

def beats(name="Angst"):
    from playsound import playsound
    import threading
    p=multiprocessing.Process(target=playsound, args=(f'{name}.mp3',))
    f=open(f"{name}.txt","r").readlines()
    for i in range(len(f)):
        f[i]=float(f[i].split("\t",1)[0])
    st=" "*120
    if True:
        p.start()
        stt=time.time()
        while p.is_alive():
            d=(time.time()-stt)-findClosestValue(f,time.time()-stt)
            if d<0.1 and d>-0.1:
                st="█"+st
                c=Fore.GREEN
            else:
                st=" "+st
                c=Fore.RED
            st=st[0:-1]
            print(c,end='')
            print(f"{st}",end='')
            print(f"\x1B[1A")
            time.sleep(0.01)
def battleground(me,name="Angst"):
    letters="qwertyuiopasdfghjklzxcvbnm"
    letters=letters+letters.upper()
    from playsound import playsound
    import threading
    p=multiprocessing.Process(target=playsound, args=(f'{name}.mp3',))
    f=open(f"{name}.txt","r").readlines()
    for i in range(len(f)):
        f[i]=float(f[i].split("\t",1)[0])
    print(f"[SHIFT + ~] to stop")
    print("\n")
    p.start()
    stt=time.time()
    c=0
    hp=me.hp()
    print(f"Track {name}, TotalBeats {len(f)}")
    while p.is_alive() and hp>0:

        ens=json.load(open("enemies.json","r",encoding="utf8"))
        enemy_id=random.choice(list(ens.keys()))
        enemy=ens[enemy_id].copy()
        enemy["level"]=max(1,int(me.level/random.randint(2,10)))
        enemy["hp"]*=enemy["level"]
        enemy["atk"]*=enemy["level"]
        enemy["rare"]*=enemy["level"]
        while p.is_alive() and hp>0 and enemy["hp"]>0:
            c+=1
            while p.is_alive() and hp>0 and enemy["hp"]>0:
                st=time.time()
                tr=random.choice(letters)

                print((f"{Fore.GREEN}"+int(hp/me.hp()*30)*"█"+f"{Fore.YELLOW} Fight with {Fore.RED}{enemy_id} HP: "+int(enemy["hp"]/(ens[enemy_id]["hp"]*enemy["level"])*30)*"█"+f"{Fore.YELLOW}").ljust(120, " "))
                print(f"{Fore.WHITE}  <  [{tr}] \x1B[3A")
                ch=msvcrt.getch().decode()
                if ch=="~":
                    hp=0
                    break
                d=(time.time()-stt)-findClosestValue(f,time.time()-stt)
                if ch==tr and d<0.2 and d>-0.2:
                    print(f"{Fore.GREEN}Boom>"+ch)
                    enemy["hp"]-=me.atk()*(1/(time.time()-st))
                    r=random.randint(-1,len(me.equipment)-1)
                    if r!= -1:exec(me.equipment[r].action())
                    break
                else:
                    print(f"{Fore.RED}Miss>"+ch)
                    hp-=enemy["atk"]*(time.time()-st)
        if hp>0 and enemy["hp"]<=0:
            me.backpack.append(drop(me.level+enemy["rare"]))
    p.terminate()
    #clean(3)
    print("\n")
    r=(c-1)/4
    print(f"Completed! Your score {round(r,2)}")
    me.level+=r
#battleground()
"""
def drop(mylevel,k):
    l=mylevel*k*random.random()*2
    r=rare(l)
    j=random.choice(json.load(open("names.json","r")))
    cls=random.choice(["Staff","Sword","Armor","Stick","Halat","Jacket","Knife","Gun","Bow"])

    #create(j,cls,{"rare":r,"type":cls,"level":l})
    o=dict2obj({"rare":r,"type":cls,"level":l,"name":j})
    if cls=="Knife" or cls=="Sword":
        o.damage=round(l*random.random()*10,2)
    if cls=="Staff" or cls=="Stick":
        o.damage=round(l*random.random()*5,2)
        o.mana=round(l*random.random()*10,2)
    if cls=="Bow":
        o.damage=round(l*random.random()*15,2)
        o.mana=round(l*random.random()*5,2)
    if cls=="Gun":
        o.damage=round(l*random.random()*20,2)

    if cls=="Armor":
        o.strength=round(l*random.random()*20,2)
    if cls=="Halat":
        o.strength=round(l*random.random()*10,2)
        o.mana=round(l*random.random()*10,2)
    if cls=="Jacket":
        o.strength=round(l*random.random()*15,2)
        o.damage=round(l*random.random()*5,2)
    o.price=round(l*random.random()*10,2)
    return o
def tutor():
    print("Welcome to the Ophelia forest!")
    print("You will hear music of nature")
    print("Press the right keys with rythm, to battle")
    r=battleground({"atk":1,"hp":100})
    print(drop(1,r))
"""
class Player(Item):
    """
    def __init__(self,name):
        self.name=name
        self.level=1
        self.hp=10
        self.atk=1
        self.luck=1
        self.max_inventory=3
        self.upgrade_stones=0
        self._inventory=()
        self._backpack=()
    """
    def __init__(self,name):
        super().__init__({"exec":"ret= 'pass'"})
        self.description = "Персонаж"
        self.comment = "Оно живое!"
        self.name=name
        self.price=0
        self.backpack=[]
        self.equipment=[]
        self.interact="ret='pass'"
        self.slots=3
    def save(self):
        with open(f"{self.name}.player","wb") as f:
            f.write(pickle.dumps(self.__dict__))
    def load(self):
        with open(f"{self.name}.player","rb") as f:
            self.__dict__.update(pickle.loads(f.read()))
    def atk(self):
        return self.deep_attr("damage")
    def hp(self):
        return self.deep_attr("strength")
    def lucky(self):
        return self.deep_attr("luck")
    def magic(self):
        return self.deep_attr("mana")
    def equip(self,i):
        if len(self.equipment)<self.slots:
            if True:
                self.equipment=self.equipment+[self.backpack[i-1]]
                self.backpack=self.backpack[:i-1]+self.backpack[i:]
            if False:
                print("Error")
        else:
            print("Inventory is full")
    def unequip(self,n):
        try:
            self.backpack=self.backpack+[self.equipment[n-1]]
            self.equipment=self.equipment[:n-1]+self.equipment[n:]
        except:
            print("Error")


    def upgrade(self,p="None"):
        if p in ["Luck", "Strength","Agility"] and self.upgrade_stones>0:
            if p=="Luck":
                self.luck+=1
            if p=="Strength":
                self.hp=self.hp*1.1
                self.max_inventory+=1
            if p=="Agility":
                self.atk=self.atk*1.5
            self.upgrade_stones-=1
        else:
            print("Luck: better loot")
            print("Strength: more inventory slots and strength")
            print("Agility: powerful base damage")
    def __repr__(self):
        s=f"{Fore.LIGHTYELLOW_EX}╔══════════════════════════╗\n"
        s+=f"║ Персонаж {self.name}\n"
        s+=f"║   {Fore.YELLOW}> Уровень {self.level}{Fore.GREEN}\n"
        s+=f"║   > Здоровье {self.hp()}{Fore.RED}\n"
        s+=f"║   > Атака {self.atk()}{Fore.BLUE}\n"
        s+=f"║   > Магия {self.magic()}{Fore.GREEN}\n"
        s+=f"║   > Удача {self.atk()}{Fore.YELLOW}\n"
        s+=f"║   > Деньги {self.price}{Fore.CYAN}\n"
        s+=f"║   > Слоты для экипировки {self.slots}{Fore.MAGENTA}\n"
        s+=f"║   > Экипировка : \n{self.equipment}\n"
        s+=f"║   > Рюкзак : \n{self.backpack}\n"
        s+=f"╚══════════════════════════╝{Fore.RESET}"
        return s


def seller(p):
    f=["Привет","Здравствуй","Йоп","Прив","Добро пожаловать"]
    s=["Я - лучший торговец на районе.","Что то продать?","За покупками?","Как поживаешь?","Новый лут?","Обращайся, если нужно что то продать.","Обращайся, если нужно что то купить."]
    nm_seller=random.choice(json.load(open("names.json","r")))
    while 1:
        os.system("cls")
        print(f"{Fore.GREEN}{nm_seller}>{Fore.YELLOW} {random.choice(f)}! {random.choice(s)}{Fore.RESET}")
        print(Fore.RED)
        print("Действия:\n")
        tasks={
            "1":"1. Магазин (обновление раз в 5 минут)",
            "2":"2. Купить предмет (нужно знать номер лота в магазине)",
            "3":"3. Продать предмет (нужно знать номер предмета в рюкзаке)",
            "4":"4. Одеть предмет в слот (нужно знать номер предмета в рюкзаке)",
            "5":"5. Снять предмет в рюкзак (нужно знать номер предмета в слоте)",
            "6":"6. Выход из меню мастера"
        }
        for i in tasks.items():
            print(i[1])
        while 1:
            i=msvcrt.getch().decode()
            if i in tasks:
                break
            else:
                print("Ошибка: Выберите номер действия из списка!")
        if i=="1":
            random.seed(time.time()-(time.time()%300))
            i1,i2,i3=drop(p.level),drop(p.level),drop(p.level)
            print(i1)
            print(i2)
            print(i3)
            input("Вернуться в меню (Enter)")
        if i=="2":
            random.seed(time.time()-(time.time()%300))
            i1,i2,i3=drop(p.level),drop(p.level),drop(p.level)
            print("Введите номер предмета из магазина для покупки (any letter to exit)> ")
            n=msvcrt.getch().decode()
            if n=="1" and i1.price<=p.price:
                p.backpack.append(i1)
                p.price-=i1.price
                print(f"Вы купили {i1.name}")
            elif n=="2" and i2.price<=p.price:
                p.backpack.append(i2)
                p.price-=i2.price
                print(f"Вы купили {i2.name}")
            elif n=="3" and i3.price<=p.price:
                p.backpack.append(i3)
                p.price-=i3.price
                print(f"Вы купили {i3.name}")
            else:
                print("Ошибка, либо такого номера нет в магазине, либо у вас не хватает денег")
            input("Вернуться в меню (Enter)")
        if i=="3":
            n=input("Введите номер предмета из рюкзака для продажи (any letter to exit)> ")
            try:
                n=int(n)
            except:
                return
            n=n-1
            try:
                p.price+=p.backpack.pop(n).price
            except:
                return
        if i=="4":
            n=input("Введите номер предмета из рюкзака для экипировки его в слот (any letter to exit)> ")
            try:
                n=int(n)-1
            except:
                return
            try:
                if len(p.equipment)>=p.slots:
                    print("Слоты заполнены, очистите один из них во вкладке")
                    return
                p.equipment.append(p.backpack.pop(n))
            except:
                return
        if i=="5":
            n=input("Введите номер предмета из рюкзака для снятия его в рюкзак (any letter to exit)> ")
            try:
                n=int(n)-1
            except:
                return
            try:
                p.backpack.append(p.equipment.pop(n))
            except:
                return
        if i=="6":
            return
        random.seed(time.time())
def shaman(p):
    f=["Приветствую тебя, путник.","Что же вас сюда занесло?","Здравствуйте.","Дверь за собой закрывать надо!","Добро пожаловать."]
    s=["Нужны магические услуги?","Хотите совершить жертвоприношение?","От вас исходит сильная аура.","Нужны услуги шамана?","Зачаровать что-то?","Тут деньги не принимают.","Обращайтесь, если нужно что-то принести в жертву."]

    ns=json.load(open("names.json","r"))
    n=random.choice(ns)
    print(f"{Fore.GREEN}{n}>{Fore.YELLOW} {random.choice(f)} {random.choice(s)}{Fore.MAGENTA}")
    print("Итак, путник, используя тёмную магию, я могу расплавить одну вещь (жертву), и слить её дух с другой вещью (хозяином).")
    print("При этом в инвентаре у тебя останется только (хозяин), а жертва передаст свой активный навык (хозяину), и бесследно исчезнет")
    print("(Хозяин) же получит два активных навыка, и будет применять их оба одновременно в бою")
    print("При жертвоприношении хозяин потеряет 10% всех своих характеристик, и 90% цены за продажу")
    print("Вы можете заливать несколько (жертв) в одного (хозяина), и даже использовать (хозяина) в качестве (жертвы)!!!")
    print("Тёмная магия позволяет создавать длинные и ветвистые цепочки активных навыков, что может дать непобедимую мощь в бою.")
    hoz=int(input("Выберите номер (хозяина) в рюкзаке"))
    print(p.backpack[hoz-1])
    zer=int(input("Выберите номер вещи, которую будем приносить в (жертву)> "))
    print(p.backpack[zer-1])
    while 1:
        w1,w2,w3=random.choice(ns),random.choice(ns),random.choice(ns)
        print(f"Если всё верно, повторите за мной заклинание {w1} {w2} {w3}>")
        inp=input(f"{p.name}>")
        if inp == f"{w1} {w2} {w3}":
            zertva=p.backpack.pop(zer-1)
            if not ("equipment" in p.backpack[hoz-1].__dict__):
                p.backpack[hoz-1].equipment=[]
            p.backpack[hoz-1].equipment.append(zertva)
            p.backpack[hoz-1].damage*=0.9
            p.backpack[hoz-1].strength*=0.9
            p.backpack[hoz-1].mana*=0.9
            p.backpack[hoz-1].luck*=0.9
            p.backpack[hoz-1].price*=0.1
            break
def occult(p):
    f=["ЙОООООУ","ХИИИИИИИЯЯЯЯЯ","БОМБОООООМ","УИИИИИИ","ОУУУ ЕЕЕ"]
    s=["ты принёс нам души?????","ГДЕ, ТВОЮ МАТЬ, ДУШИ ГРЕШНИКОВ?","ПИУ ПИУ ПИУ.","ВРРУМ ВРУУМ","БЭ БЭУ?","повелитель ждёт вас!","хозяин ожидает."]

    ns=json.load(open("names.json","r"))
    n=random.choice(ns)
    print(f"{Fore.GREEN}{n}>{Fore.YELLOW} {random.choice(f)}, {random.choice(s)}{Fore.MAGENTA}")
    print(f"{Fore.RED}Неизвстное божетво{Fore.RESET}> Ты можешь использовать мою силу, для превращения чужой {Fore.BLUE}души{Fore.RESET} в предмет. Поскольку в вашем бренном мире в основе всего лежат {Fore.BLUE}душы{Fore.RESET}, а оружия, которыми вы проливаете {Fore.RED}кровь{Fore.RESET} - материальное воплощение {Fore.BLUE}души{Fore.RESET}. Значит я могу {Fore.RED}ПРЕВРАТАИТЬ ЧЕЛОВЕКА В ОРУЖИЕ, С ЕГО ХАРАКТЕРИСТИКАМИ АХАХАХАХАХАХАХХААХХАХА{Fore.RESET}")
    nm=input("Введите имя вашего существующего персонажа, который будет отдан божеству: ")
    try:
        with open(f"{nm}.player","rb") as f:
            p2=pickle.loads(f.read())
        rand_i=Item({"exec":"pass"})
        rand_i.__dict__.update(p2)
        rand_i.rare=rare(rand_i.level)
        p.backpack.append(rand_i)
        os.system(f"del {nm}.player")
    except:
        print(f"{Fore.RED}Неизвстное божетво{Fore.RESET}> {Fore.RED}ЫААЫЫАЫАЫАЫ{Fore.RESET}, ты облажался !!!! что-то ты сделал не так!!! ЛИБО ТЫ ВЫБРАЛ {Fore.RED}НЕСУЩЕСТВУЮЩЕГО ИГРОКА{Fore.RESET}, либо ты пыташься меня {Fore.RED}ОБМАНУТЬ{Fore.RESET}")

def menu():
    from playsound import playsound
    import threading
    p=multiprocessing.Process(target=playsound, args=(f'Viol.mp3',))
    f=open(f"Viol.txt","r").readlines()
    for i in range(len(f)):
        f[i]=float(f[i].split("\t",1)[0])
    st=" "*120
    p.start()
    stt=time.time()
    print(f"{Fore.GREEN}Game by {Fore.YELLOW}DroidX{Fore.RESET} P.S. Рекомендуются наушники")
    l=""
    for i in range(43):
        l+="═"
        print(l)
        print("\x1B[2A")
        time.sleep(0.1)
    print(f"\x1B[2A")
    print(f"                 {Fore.MAGENTA}Ophelia{Fore.RED}                    ")
    print("\n")
    t=r"""(•_•)-----(-_•)-----(•_-)-----(-_-)""".split("-----")
    m=r"""<) )╯_---_<) )-_---_<) )/_---_\( (>_---_-( (>_---_-( (-_---_~( (~""".split("_---_")
    dw=r""" | \ ----- / \ ----- / | ----- < | ----- | > ----- < \ ----- / > """.split("-----")
    man=[t[0],m[0],dw[0]]
    
    while p.is_alive():
        d=(time.time()-stt)-findClosestValue(f,time.time()-stt)
        if d<0.01 and d>-0.01:
            r=random.randint(0,2)
            if r==0:
                man[0]=random.choice(t)
            if r==1:
                man[1]=random.choice(m)
            if r==2:
                man[2]=random.choice(dw)
        print(f"{Fore.RED}    "+"\n    ".join(man)+f"\n{Fore.GREEN}  Нажмите Enter, для продолжения\n")
        print(f"\x1B[6A")
        time.sleep(0.04)
        if keyboard.is_pressed('enter'):
            os.system("cls")
            break
    input()
    n=input("Введите имя персонажа (старое, если персонаж был создан ранее)> ")
    player=Player(n)
    while 1:
        os.system("cls")
        print(Fore.YELLOW)
        print("Действия:\n")
        tasks={
            "1":f"1. В {Fore.LIGHTBLUE_EX}портал{Fore.YELLOW}",
            "2":"2. Информация о персонаже",
            "3":"3. Мастер (магазин + инвентарь)",
            "4":f"4. Шаман {Fore.MAGENTA}(внимание, тёмная магия - возможны програмные ошибки){Fore.YELLOW}",
            "5":f"5. Культисты безимянного бога {Fore.RED}(внимание, сатанизм - возможны серьёзные баги){Fore.YELLOW}",
            "6":f"6. {Fore.GREEN}Сохранить{Fore.YELLOW} текущего персонажа",
            "7":f"7. {Fore.CYAN}Загрузить{Fore.YELLOW} текущего персонажа",
            "8":f"8. {Fore.RED}Выход{Fore.YELLOW} из игры",
            "=":'=. Консоль разработчика'
        }
        for i in tasks.items():
            print(i[1])
        while 1:
            i=msvcrt.getch().decode()
            if i in tasks:
                break
            else:
                print("Ошибка: Выберите номер действия из списка!")
        if i=="1":
            p.terminate()
            battleground(player)
            input("Вернуться в меню (Enter)")
        if i=="2":
            print(player)
            input("Вернуться в меню (Enter)")
        if i=="3":
            print()
            seller(player)
            input("Вернуться в меню (Enter)")
        if i=="4":
            print()
            shaman(player)
            input("Вернуться в меню (Enter)")
        if i=="5":
            print()
            occult(player)
            input("Вернуться в меню (Enter)")
        if i=="6":
            player.save()
        if i=="7":
            player.load()
        if i=="8":
            p.terminate()
            print("Ctrl+C если выход не завершился в автоматическом режиме")
            sys.exit()
        if i=="=":
            import pdb; pdb.set_trace(header=f"{Fore.MAGENTA}Строго для использования разработчиком! {Fore.RED}cont{Fore.MAGENTA} для выхода из консоли{Fore.GREEN}")
if __name__ == "__main__":
    menu()